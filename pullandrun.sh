cat ./dockerpw.txt | docker login --username iamaelephant --password-stdin
docker pull iamaelephant/backlogger:latest

docker kill backlogger
docker rm backlogger

docker run --name backlogger -d -p 8080:80 iamaelephant/backlogger:latest
