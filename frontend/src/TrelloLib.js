class TrelloLib {

    constructor(userService) {
        this.userService = userService;
    }

    sendToTrello(game, listId) {
        var data = {
            name: game.name,
            pos: 'top'
        }
        Trello.post(`/cards?idList=${listId}`, data)
            .done((card) => {
                Trello.post(`/cards/${card.id}/attachments`, {url: game.headerImage});
            });
    

    }

    createBoard(userEmail, name) {
        /* global Trello */
        var mainPromise = Trello.post(`/boards/?name=${name}&defaultLists=false`);
        mainPromise.done((data) => {
            var backlog   = Trello.post(`/lists?idBoard=${data.id}&name=Backlog`);
            var playing   = Trello.post(`/lists?idBoard=${data.id}&name=Playing`);
            var done      = Trello.post(`/lists?idBoard=${data.id}&name=Done`);

            /* global jQuery */ 
            jQuery.when(backlog, playing, done).done(
                (backlog, playing, done) => {
                    this.userService.setTrelloListIds(userEmail, {
                        backlog: backlog[0].id,
                        playing: playing[0].id,
                        done: done[0].id
                    })
                }
            );
        });
        return mainPromise;
    }

    getBoard(id) {
        /* global Trello */
        return Trello.get(`/boards/${id}`);
    }
}

export default TrelloLib;