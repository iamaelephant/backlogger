class UserService {

    setTrelloBoard(userEmail, trelloBoardId) {
        /* global jQuery */
        return jQuery.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: `${window.baseApiUrl}api/user/${userEmail}/trelloBoard`,
            dataType: 'json',
            data: JSON.stringify({trelloBoardId: trelloBoardId})
        });
    }

    setTrelloListIds(userEmail, trelloLists) {
        return jQuery.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: `${window.baseApiUrl}api/user/${userEmail}/trelloLists`,
            dataType: 'json',
            data: JSON.stringify({trelloLists})
        });
    }

    createUser(email) {
        return jQuery.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: `${window.baseApiUrl}api/user/`,
            dataType: 'json',
            data: JSON.stringify({email: email})
        })
    }

}

export default UserService;