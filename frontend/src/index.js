import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

window.baseApiUrl = process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:5000/';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
