
class Steam {

    setSteamId(userEmail, steamId) {
        /* global jQuery */
        return jQuery.ajax({
            type: 'POST',
            contentType:"application/json; charset=utf-8",
            url: `${window.baseApiUrl}api/user/${userEmail}/steamId`,
            dataType: 'json',
            data: JSON.stringify({ steam64Id: steamId })
        });
    }

    getSteamProfile(steamId) {
        /* global jQuery */
        return jQuery.ajax({
            type: 'GET',
            contentType:"application/json; charset=utf-8",
            url: `${window.baseApiUrl}api/user/${steamId}/steamProfile`
        });

        /*
        {
            "steamId":"76561197966632806",
            "personaName":"Spicy McHaggis",
            "profileUrl":"http://steamcommunity.com/id/iamaelephant/",
            "avatar":"https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/2c/2c98008093751503ce8ec992634d0124652c376f.jpg",
            "realName":"Martin Doms",
            "locCountryCode":"AU"
        }
        */
    }

}

export default Steam;