import React, { Component } from 'react';
import TrelloLib from '../TrelloLib';
import UserService from '../UserService';

export default class TrelloAuth extends Component {

    constructor() {
        super();
        this.state = {
            trelloBoardId: undefined
        }

        this.userService = new UserService();
        this.trelloLib = new TrelloLib(this.userService);
        this.TRELLO_BOARD_NAME = "Steam backlog";
    }

    authenticationSuccess = function () {
        // create the user if it doesn't exist
        var me = Trello.members.get("me");
        me.done((meResult) => {
            this.userService.createUser(meResult.email).done((createdUser) => {
                // if the user already has a board make sure we can get it
                if (createdUser.trelloBoardId) {
                    var getBoardPromise = this.trelloLib.getBoard(createdUser.trelloBoardId);
                    getBoardPromise.done((board) => {
                        this.props.authSuccess(this.state.trelloBoardId, createdUser);
                    });
                }
                else {
                    // otherwise create one and assign it to the user
                    var createBoardPromise = this.trelloLib.createBoard(createdUser.email, this.TRELLO_BOARD_NAME);
                    createBoardPromise.done((createBoardResponse) => {
                    this.userService.setTrelloBoard(createdUser.email, createBoardResponse.id)
                        .done(() => {
                            this.props.authSuccess(createBoardResponse.id, createdUser);
                        })
                    });
                }
            });
        });
    };

    authenticationFailure() { 
        console.log('Failed Trello authentication'); 
    };

    authenticate() {
        /* global Trello */
        return Trello.authorize({
            type: 'popup',
            name: 'Backlogger Steam Backlog',
            scope: {
                read: 'true',
                write: 'true',
                account: 'true'
            },
            expiration: 'never',
            success: (a,b,c) => this.authenticationSuccess(),
            error: () => this.authenticationFailure()
        });
    }

    render() {
        return (
        <div className='trello-auth-view'>
            <a href="#trello-auth" onClick={() => this.authenticate()}>Click here to get started</a>
        </div>
        );
    }

}