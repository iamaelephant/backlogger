import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from './Constants';

const style = {
  color: 'white',
  padding: '1rem',
  textAlign: 'center',
  fontSize: '1rem',
  lineHeight: 'normal',
  border: '1px solid black'
};

const boxTarget = {
  drop(props) {
    return { name: 'DropTarget', action: props.action };
  },
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
    }
}


class DragTarget extends Component {
  static propTypes = {
    position: PropTypes.string.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    description: PropTypes.string.isRequired
  };

  render() {
    const { canDrop, isOver, connectDropTarget } = this.props;
    const isActive = canDrop && isOver;
    let backgroundColor = '#222';
    if (isActive) {
      backgroundColor = 'darkgreen';
    } else if (canDrop) {
      backgroundColor = 'darkkhaki';
    }

    var className = 'App-grid-' + this.props.position;

    return connectDropTarget(
      <div style={{ 
        ...style, backgroundColor
      }} className={className}>
        {isActive ?
          'Release to drop' :
          this.props.description
        }
      </div>,
    );
  }
}

export default DropTarget(ItemTypes.CARD, boxTarget, collect)(DragTarget)