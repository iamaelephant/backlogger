import React from 'react';

const SteamProfile = ({ avatarFull, personaName, profileUrl, realName, locCountryCode }) => {
    let cardStyle = {
        width: '320px',
        height: '400px',
        border: '1px solid  black',
        backgroundColor: 'gray',
        backgroundImage: `url('./images/steam-bg.jpg')`,
        backgroundSize: 'auto 100%',
        borderRadius: '5px',
        color: 'white',
    }

    let avatarStyle = {
        width: '184px',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '20px'
    }

    let bioStyle = {
        fontFamily: 'sans-serif',
        margin: '12px',
        textAlign: 'left'
    }

    let linkStyle = {
        color: 'inherit',
        textDecoration: 'inherit'
    }

    return (
        <div className="steam-profile" style={cardStyle}>
            <div style={avatarStyle}>
                <a href={profileUrl} target="_blank" style={linkStyle}>
                    <img src={avatarFull || "./images/avatar-empty.jpg"} alt="avatar" />
                </a>
            </div>
            <div style={bioStyle}>
                <a href={profileUrl} target="_blank" style={linkStyle}>
                    <p>{personaName}</p>
                </a>
                <ul>
                    {realName && <li>Real name: {realName}</li>}
                    {locCountryCode && <li>Country: {locCountryCode}</li>}
                </ul>
            </div>
        </div>
    )
}

export default SteamProfile;