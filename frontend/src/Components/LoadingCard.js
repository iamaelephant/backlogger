import React from 'react';
import './LoadingCard.css';

const LoadingCard = () => {
    let cardStyle = {
        width: '320px',
        height: '400px',
        border: '1px solid  black',
        backgroundColor: '#1B2838',
        borderRadius: '5px'
    }


    return (
        <div style={cardStyle} className="loading-card">
            <div className="spinner">
            </div>
        </div>
    )
}

export default LoadingCard;