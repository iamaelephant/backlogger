import React, { Component } from 'react';
import Card from './Card';

class CardDragPreview extends Component {

    render() {
        return (

            <Card { ...this.props } />

        );
    }

}

export default CardDragPreview;