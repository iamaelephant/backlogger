import React, { Component } from 'react';

export default class SteamAuthInput extends Component {

    constructor() {
        super();
        this.state = {
            steamIdInput: ''
        }
    }


    handleSteamIdChange(event) {
        this.setState({steamIdInput: event.target.value});
    }

    handleSteamIdKeyPress(event) {
        if (event.keyCode === 13) {
            this.props.submitInput && this.props.submitInput(this.state.steamIdInput);
        }
    }

    render() {
        return (
            <div className="steam-auth-view">
                <p className="announce">
                    Now enter your Steam username or Steam64Id
                </p>
                <input type="text" value={this.state.steamIdInput} onChange={(e) => this.handleSteamIdChange(e)} onKeyDown={(e) => this.handleSteamIdKeyPress(e)} />
            </div>
        );
    }
}