import React from 'react';
import './LoadingCard.css';

const LoadingSteamProfile = () => {
    let cardStyle = {
        width: '320px',
        height: '400px',
        border: '1px solid  black',
        backgroundColor: '#222',
        backgroundImage: `url('./images/steam-bg.jpg')`,
        backgroundSize: 'auto 100%',
        borderRadius: '5px',
        color: 'white',
    }

    return (
        <div className="steam-profile" style={cardStyle}>
            <div className="spinner">
            </div>
        </div>
    )
}

export default LoadingSteamProfile;