import React from 'react';

const EmptyCard = () => {
    let cardStyle = {
        width: '320px',
        height: '400px',
        border: '1px solid  black',
        backgroundColor: '#1B2838',
        borderRadius: '5px',
        position: 'relative'
    }

    let imageStyle = {
        maxHeight:'100%',
        maxWidth:'100%',
        width: 'auto',
        height: 'auto',
        position: 'absolute',
        top: '0',
        bottom: '0',
        left: '0',
        right: '0',
        margin: 'auto',
        opacity: '0.5'
    }

    return (
        <div style={cardStyle} className="empty-card">
            <img src="./images/avatar-empty.jpg" alt="empty card" style={imageStyle} />
        </div>
    )
}

export default EmptyCard;