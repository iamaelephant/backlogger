import React from 'react';

const Card = ({ name, headerImage, background, playedTime }) => {
    let cardStyle = {
        width: '320px',
        height: '400px',
        border: '1px solid  black',
        backgroundColor: 'gray',
        backgroundImage: `url(${background})`,
        backgroundSize: 'auto 100%',
        borderRadius: '5px',
        color: 'white',
    }

    let titleStyle = {
        textAlign: 'left',
        marginLeft: '10px',
        fontSize: '1.3em'
    }

    let playedTimeStyle = {
        textAlign: 'left',
        marginLeft: '10px',
        fontSize: '1.1em'
    }

    let cardImageStyle = {
        width: '100%'
    }

    if (!playedTime) playedTime = 0;
    let hours = Math.floor(playedTime / 60);
    let minutes = playedTime % 60;
    let friendlyTime = `${hours} hour${hours === 1 ? "" : "s"}, ${minutes} minute${minutes === 1 ? "" : "s"}`;
    if (!hours) {
        friendlyTime = `${minutes} minutes`;
    }

    return (
    <div style={cardStyle} className="card">
        <div>
            <img style={cardImageStyle} src={headerImage} alt="" />
            <p style={titleStyle}>
                { name }
            </p>
            <p style={playedTimeStyle}>Played time: { friendlyTime }</p>
        </div>
    </div>
    );
}


export default Card;