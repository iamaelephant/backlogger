import React, { Component } from 'react';
import Steam from '../Steam';
import SteamProfile from './SteamProfile';
import SteamAuthInput from './SteamAuthInput';
import LoadingSteamProfile from './LoadingSteamProfile';


export default class SteamAuthContainer extends Component {

    constructor() {
        super();
        this.state = {loading: false, loaded: false};
        this.steam = new Steam();
    }

    submitInput(steamIdInput) {
        this.setState({loading: true});
        let profilePromise = this.steam.getSteamProfile(steamIdInput);
        profilePromise.done((profile,a,b) => {
            this.setState({ ...profile, loading:false, loaded: true });
        })
        .fail((e) => {
            this.setState({loaded: false, loading: false});
        });
    }

    selectProfile() {
        let result = this.steam.setSteamId(this.props.userEmail, this.state.steamId);
        result
            .done((data) => {
                this.props.authSuccess(data.games);
            })
            .fail((data) => {
                this.setState({loading:false, loaded:false});
            });
    }

    render() {
        console.log('profile');
        console.log(this.state);
        return (
            <div className="steam-auth-view">
                <SteamAuthInput submitInput={(i) => this.submitInput(i)} />
                {(this.state.loaded || this.state.loading) &&
                    <div className="profileContainer">
                        {this.state.loading ?
                            <LoadingSteamProfile />    
                        :   <SteamProfile {...this.state} />
                        }
                        <button onClick={() => this.selectProfile()}>This is me!</button>
                    </div>
                }

            </div>
        );
    }
}