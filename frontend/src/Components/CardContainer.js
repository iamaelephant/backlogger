import React, { Component } from 'react';
import LoadingCard from './LoadingCard';
import DraggableCard from './DraggableCard';

class CardContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        }
    }

    loaded() {
        return !!this.state.loaded;
    }

    componentDidMount() {
        if (!this.loaded()) {
            /* global jQuery */
            jQuery.ajax({
                type: 'GET',
                contentType:"application/json; charset=utf-8",
                url: `${window.baseApiUrl}api/game/${this.props.gameId}?userEmail=${this.props.userEmail}`
            })
            .done((gameData) => {
                this.setState({
                    loaded: true, 
                    name: gameData.name, 
                    headerImage: gameData.headerImage, 
                    background: gameData.backgroundImage,
                    playedTime: gameData.playedTime,
                    gameId: gameData.gameId
                });
            })
            .fail((e) => {
                this.props.discard && this.props.discard(this.props.game);
            });
        }
    }
    
    render() {
        const { loaded } = this.state;

        return (
            <div className="card-container">
                {loaded ? 
                    <DraggableCard {... this.state } />
                    :
                    <LoadingCard />}
            </div>
        );
    }
}

export default CardContainer;