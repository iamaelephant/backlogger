import React, { Component } from 'react';
import { DragSource } from 'react-dnd';
import PropTypes from 'prop-types';
import { ItemTypes } from '../Constants';
import Card from './Card';

const cardSource = {
    beginDrag(props, monitor, component) {
        return { ...component.props };
    },
    
    endDrag(props, monitor, component) {
        const dropResult = monitor.getDropResult();
        if (dropResult && dropResult.action) {
            dropResult.action({
                name: component.props.name,
                headerImage: component.props.headerImage,
                gameId: props.gameId
            });
        }
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    };
}

const propTypes = {
    name: PropTypes.string,
    headerImage: PropTypes.string,
    background: PropTypes.string,
    playTime: PropTypes.number,

    // Injected by React DnD:
    isDragging: PropTypes.bool.isRequired,
    connectDragSource: PropTypes.func.isRequired
};


class DraggableCard extends Component {
    render() {
        const { isDragging, connectDragSource } = this.props;

        return connectDragSource (
            <div style={{ opacity: isDragging ? 0.05 : 1 }}>
                <Card {... this.props } />
            </div>
        );
    }
}

DraggableCard.propTypes = propTypes;
export default DragSource(ItemTypes.CARD, cardSource, collect)(DraggableCard);