import React from 'react';
import EmptyCard from './EmptyCard';
import './Deck.css';

import { CSSTransitionGroup } from 'react-transition-group';

const deckStyle = {
    gridRow: '2',
    gridColumn: '2',
    padding: '10px',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '100%'
}

const bottomCard = {
    position: 'absolute'
}

const middleCard = {
    position: 'absolute',
    marginLeft: '16px',
    marginTop: '16px'
}

const topCard = {
    position: 'absolute',
    marginLeft: '32px',
    marginTop: '32px'
}

class Deck extends React.Component {

    constructor() {
        super()
        document.addEventListener("keydown", (event) => this.handleKeyPress(event), false);
    }

    handleKeyPress(event) {
        this.props.keyDown && this.props.keyDown(event, this.props.cards[0]);
    }

    renderChildren(children) {
        if (!children) {
            return <EmptyCard />
        }
        if (children instanceof Array) {
            if (children.length === 0) {
                return <EmptyCard />
            }
            else if (children.lenth === 1) {
                return children[0];
            }
            else if (children.length === 2) {
                return (
                    <span>
                        <div style={middleCard} className="middleCard">
                            {children[1]}
                        </div>
                        <div style={topCard} className="topCard">
                            {children[0]}
                        </div>
                    </span>
                );
            }
            else {
                return (
                    <span>
                        <CSSTransitionGroup
                            transitionName="bottom-card-move"
                            transitionEnterTimeout={300}
                            transitionLeaveTimeout={10}
                        >
                            <div style={bottomCard} className="bottomCard" key={children[2].props.gameId}>
                                {children[2]}
                            </div>
                        </CSSTransitionGroup>
                        <CSSTransitionGroup
                            transitionName="middle-card-move"
                            transitionEnterTimeout={300}
                            transitionLeaveTimeout={200}
                        >
                            <div style={middleCard} className="middleCard" key={children[1].props.gameId}>
                                {children[1]}
                            </div>
                        </CSSTransitionGroup>
                        <CSSTransitionGroup
                            transitionName="top-card-move"
                            transitionEnterTimeout={300}
                            transitionLeaveTimeout={200}
                        >
                            <div style={topCard} className="topCard" key={children[0].props.gameId}>
                                {children[0]}
                            </div>
                        </CSSTransitionGroup>
                    </span>
                );
            }
        }
        else {
            return children;
        }
    }

    render() {
        return (
            <div style={deckStyle} className="deck">
                {this.renderChildren(this.props.children)}
            </div>
        );
    }

}

export default Deck;