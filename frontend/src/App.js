import React, { Component } from 'react';
import { default as TouchBackend } from 'react-dnd-touch-backend';
import { DragDropContext } from 'react-dnd';
import Deck from './Components/Deck'
import DragTarget from './DragTarget'
import './App.css';
import TrelloLib from './TrelloLib';
import SteamAuthContainer from './Components/SteamAuthContainer.js';
import TrelloAuth from './Screens/TrelloAuthView.js';
import UserService from './UserService.js';
import CustomDragLayer from './DragAndDrop/CustomDragLayer.js';
import CardContainer from './Components/CardContainer';

class App extends Component {

  constructor() {
    super();
    this.state = {
      cards: [],
      steamId: '',
      userId: 0,
      trelloBoardId: undefined,
      steamAuthenticated: false
    };

    this.trelloLib = new TrelloLib(new UserService());
  }

  steamAuthSuccess(games) {
    this.setState({cards: games, steamAuthenticated: true});
  }

  trelloAuthSuccess(trelloBoardId, user) {
    if (user.steam64Id) {
      this.setState({steamAuthenticated: true, cards: user.games});
    }
    this.setState({trelloBoardId:trelloBoardId, userId: user.id, user: user, trelloAuthenticated: true});
  }

  discard(game) {
    this.setState({cards: this.state.cards.filter(c => c.gameId !== game.gameId)});
    /* global jQuery */
    jQuery.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: `${window.baseApiUrl}api/game/${game.gameId}/${this.state.user.email}/done`
    });
  }

  sendToTrello(game, list, userId) {
    if (game && game.headerImage) {
      this.trelloLib.sendToTrello(game, list);
      this.discard(game);
    }
    else {
      console.log(game);
      console.log('Can\'t send unhydrated game');
    }
  }

  handleKeyPress(event, game) {
    const key = event.key;
    if (key === "ArrowRight") {
      this.sendToTrello(game, this.state.user.listIdBacklog);
    }
    else if (key === "ArrowUp") {
      this.discard(game);
    }
    else if (key === "ArrowLeft") {
      this.sendToTrello(game, this.state.user.listIdPlaying);
    }
    else if (key === "ArrowDown") {
      this.sendToTrello(game, this.state.user.listIdDone);
    }
  }

  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  render() {
    if (this.state.steamAuthenticated && this.state.trelloAuthenticated) {
      return (
        <div className="App">
          <CustomDragLayer />
          <div className="App-grid">
            <Deck 
              cards={this.state.cards} 
              keyDown={(event, game) => this.handleKeyPress(event, game)}
            >
              {this.state.cards && this.state.cards.map(c => 
                <CardContainer 
                    game={c}
                    name={c.name} 
                    key={c.gameId} 
                    gameId={c.gameId} 
                    headerImage={c.headerImage} 
                    isActive={true}
                    discard={(game) => this.discard(game)} 
                    userEmail={this.state.user.email}
                />
              )}
            </Deck>
            <DragTarget position='top' description='Discard this game' action={(game) => this.discard(game)} />
            <DragTarget position='left' description="I'm playing this now" action={(game) => this.sendToTrello(game, this.state.user.listIdPlaying)} />
            <DragTarget position='right' description="I want to play this game" action={(game) => this.sendToTrello(game, this.state.user.listIdBacklog)} />
            <DragTarget position='bottom' description="I have played this game" action={(game) => this.sendToTrello(game, this.state.user.listIdDone)} />
          </div>
        </div>
      );
    } else if (this.state.trelloAuthenticated) {
      return (
        <div className="App">
          <SteamAuthContainer userEmail={this.state.user.email} authSuccess={(games) => this.steamAuthSuccess(games)} />
        </div>
      );
    } 
    else {
      return (
        <div className="App">
        <h1>You are in {process.env.NODE_ENV} mode (HAHAHAHAHA)</h1>
            <TrelloAuth authSuccess={(trelloBoardId, user) => this.trelloAuthSuccess(trelloBoardId, user)} />
        </div>
      );
    }

  }
}


export default DragDropContext(TouchBackend({enableMouseEvents: true}))(App);
