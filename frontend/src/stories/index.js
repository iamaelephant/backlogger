import React from 'react';

import { DragDropContextProvider } from 'react-dnd';
import { default as TouchBackend } from 'react-dnd-touch-backend';
import HTML5Backend from 'react-dnd-html5-backend';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Card from '../Components/Card';
import DraggableCard from '../Components/DraggableCard';
import LoadingCard from '../Components/LoadingCard';
import SteamAuthInput from '../Components/SteamAuthInput';
import SteamProfile from '../Components/SteamProfile';
import LoadingSteamProfile from '../Components/LoadingSteamProfile';
import Deck from '../Components/Deck';


storiesOf('Card', module)
  .add('regular card, no play time', () => 
    <Card 
      name="game name" 
      headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/359550/header.jpg"
      background="http://cdn.akamai.steamstatic.com/steam/apps/730/page_bg_generated_v6b.jpg?t=1513742714"
      /> )
    .add('regular card, less than an hour playtime', () => 
      <Card 
        name="game name" 
        headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/359550/header.jpg"
        background="http://cdn.akamai.steamstatic.com/steam/apps/730/page_bg_generated_v6b.jpg?t=1513742714"
        playedTime="59"
        /> )
      .add('regular card, one hour play time', () => 
        <Card 
          name="game name" 
          headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/359550/header.jpg"
          background="http://cdn.akamai.steamstatic.com/steam/apps/730/page_bg_generated_v6b.jpg?t=1513742714"
          playedTime="60"
          /> )
        .add('regular card, 1:59 playtime', () => 
          <Card 
            name="game name" 
            headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/359550/header.jpg"
            background="http://cdn.akamai.steamstatic.com/steam/apps/730/page_bg_generated_v6b.jpg?t=1513742714"
            playedTime="119"
            /> )          
  .add('draggable card', () =>
    <DragDropContextProvider backend={HTML5Backend}>
      <DraggableCard 
        name="game name" 
        headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/359550/header.jpg" 
        background="http://cdn.akamai.steamstatic.com/steam/apps/730/page_bg_generated_v6b.jpg?t=1513742714"
        gameId="123" 
      />
    </DragDropContextProvider>
  )
  .add('loading card', () => 
    <LoadingCard />
  )

storiesOf('Steam auth input', module)
  .add('ask for input', () =>
    <SteamAuthInput userEmail="iamaelephant@gmail.com" authSuccess={() => {}} />
  )

storiesOf('Steam profile', module)
  .add('empty profile', () =>
    <SteamProfile />
  )
  .add('profile with avatar and name', () =>
    <SteamProfile 
      personaName='Spicy McHaggis'
      avatarFull='https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/2c/2c98008093751503ce8ec992634d0124652c376f_full.jpg' />
  )
  .add('profile with all details', () =>
    <SteamProfile 
      personaName='Spicy McHaggis'
      profileUrl="https://google.com"
      avatarFull='https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/2c/2c98008093751503ce8ec992634d0124652c376f_full.jpg'
      realName="Martin Doms"
      locCountryCode="AU" />
  )
  .add('loading steam profile', () =>
    <LoadingSteamProfile />
  )

storiesOf('Deck', module)
  .add('empty deck', () =>
    <Deck />
  )
  .add('deck with one card', () =>
    <DragDropContextProvider backend={HTML5Backend}>
      <Deck>
        <DraggableCard 
          name="game name" 
          headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/359550/header.jpg" 
          background="http://cdn.akamai.steamstatic.com/steam/apps/730/page_bg_generated_v6b.jpg"
          gameId="123" 
        />
      </Deck>
    </DragDropContextProvider>
  )
  .add('deck with multiple cards', () =>
    <DragDropContextProvider backend={HTML5Backend}>
      <Deck>
        <DraggableCard 
          name="game name" 
          headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/359550/header.jpg" 
          background="http://cdn.akamai.steamstatic.com/steam/apps/730/page_bg_generated_v6b.jpg"
          gameId="123" 
        />
        <DraggableCard 
          name="PUBG" 
          headerImage="http://cdn.edgecast.steamstatic.com/steam/apps/578080/header.jpg"
          background="http://cdn.akamai.steamstatic.com/steam/apps/578080/page_bg_generated_v6b.jpg"
          gameId="123" 
        />
      </Deck>
    </DragDropContextProvider>
  )