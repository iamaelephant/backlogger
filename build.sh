mkdir -p ./out
dotnet restore ./BackLogger.Web/BackLogger.Web.csproj
dotnet publish -o ../out ./BackLogger.Web/BackLogger.Web.csproj
mkdir -p ./out/wwwroot
pushd frontend && npm install && npm run build && popd
cp -r ./frontend/build/** ./out/wwwroot
