FROM microsoft/aspnetcore

COPY ./out /var/aspnetcore/backlogger
WORKDIR /var/aspnetcore/backlogger

EXPOSE 80

ENTRYPOINT ["dotnet", "BackLogger.Web.dll"]
