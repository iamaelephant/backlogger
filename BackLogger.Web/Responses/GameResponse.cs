using System.Collections.Generic;

namespace BackLogger.Web.Responses
{
    public class GameResponse
    {
        public GameData Data { get; set; }
        public bool Success { get; set; }
    }

    public class GameData
    {
        public string Name { get; set; }
        public int Steam_AppId { get; set; }
        public string Header_Image { get; set; }
        public string Background { get; set; }
        public List<CategoryData> Categories { get; set; }
    }

    public class CategoryData
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}