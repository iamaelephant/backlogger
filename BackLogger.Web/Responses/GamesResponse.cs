using System.Collections.Generic;

namespace BackLogger.Web.Responses
{
    public class GamesResponse
    {
        public Response response { get; set; }

        public class Game
        {
            public int appid { get; set;}
            public int playtime_forever { get; set;}
        }

        public class Response
        {
            public int game_count { get;set;}
            public IEnumerable<Game> games { get; set;}
        }
    }
}
