using BackLogger.Web.Model;
using ServiceStack.Redis;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using ServiceStack.Redis.Generic;

namespace BackLogger.Web.Data {

    public class BackloggerRedis {
        private IConfiguration Configuration { get; }

        public BackloggerRedis(IConfiguration configuration) {
            Configuration = configuration;
        }

        private IRedisClient GetClient()
        {
            var connectionString = $"{Configuration["redis:host"]}:{Configuration["redis:port"]}";
            System.Console.WriteLine($"Using redis connection string {connectionString}");
            var manager = new RedisManagerPool(connectionString);

            return manager.GetClient();
        }
        public User GetUser(string email) {
            using (var client = GetClient())
            {
                return client.Get<User>("users/" + email);
            }
        }

        public void SetUser(string email, User user) {
            using (var client = GetClient())
            {
                client.Set($"users/{email}", user);
            }
        }

        public IEnumerable<Game> GetAllGames()
        {
            using (var client = GetClient())
            {
                IRedisTypedClient<Game> redis = client.As<Game>();
                var hash = redis.GetHash<int>("games");
                return redis.GetHashValues(hash);
            }            
        }

        public void SetGames(IEnumerable<Game> games)
        {
            using (var client = GetClient())
            {
                IRedisTypedClient<Game> redis = client.As<Game>();
                foreach (var game in games)
                {
                    var hash = redis.GetHash<int>("games");
                    redis.SetEntryInHashIfNotExists<int>(hash, game.GameId, game);
                }
            }    
        }

        public Game GetGame(int gameId)
        {
            using (var client = GetClient())
            {
                IRedisTypedClient<Game> redis = client.As<Game>();
                var hash = redis.GetHash<int>("games");
                return redis.GetValueFromHash(hash, gameId);
            }   
        }

        public Game SetGame(Game game)
        {
            using (var client = GetClient())
            {
                IRedisTypedClient<Game> redis = client.As<Game>();
                var hash = redis.GetHash<int>("games");
                redis.SetEntryInHash(hash, game.GameId, game);
                return game;
            }   
        }
    }

}