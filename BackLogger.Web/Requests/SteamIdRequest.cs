namespace BackLogger.Web.Requests
{
    public class SteamIdRequest
    {
        public string Steam64Id { get; set; }
    }
}
