using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BackLogger.Web.Data;
using BackLogger.Web.Model;
using BackLogger.Web.Responses;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BackLogger.Web.Services
{
    public class GamesService
    {
        private static string ApiKey = Environment.GetEnvironmentVariable("STEAM_API_KEY");

        private readonly BackloggerRedis redis;
        private readonly HttpClient httpClient;
        private readonly ILogger<GamesService> logger;

        public GamesService(BackloggerRedis redis, HttpClient httpClient, ILogger<GamesService> logger)
        {
            this.redis = redis;
            this.httpClient = httpClient;
            this.logger = logger;
        } 

        public async Task<Game> GetGame(int gameId)
        {
            return await Task.Run(() => redis.GetGame(gameId));
        }

        public async Task<Game> UpdateGame(Game game)
        {
            return await Task.Run(() => redis.SetGame(game));
        }

        public async Task<Game> AddGame(Game game)
        {
            return await Task.Run(() => redis.SetGame(game));
        }
        
        public async Task<IEnumerable<Model.Game>> GetGamesForUser(User user)
        {
            // for now we'll go to the Steam API every time. Re-evaluate if this turns out to be a problem
            var fromSteam = await GetGamesForSteamId(user.Steam64Id);

            logger.LogInformation("Fetched games for {userId}", user.Id);

            var games = redis.GetAllGames().ToList();
            var gamesDictionary = games.ToDictionary(game => game.GameId);
            IDictionary<int, UserGame> userGamesDictionary = new Dictionary<int, UserGame>();
            if (user.UserGames == null)
            {
                user.UserGames = new List<UserGame>();
            }

            userGamesDictionary = user.UserGames.ToDictionary(ug => ug.GameId);

            logger.LogInformation("Sorting {gameCount} games for user", fromSteam.response.games.Count());
            foreach (var steamGame in fromSteam.response.games)
            {
                if (!gamesDictionary.ContainsKey(steamGame.appid))
                {
                    games.Add(new Game() { GameId = steamGame.appid});
                }
                if (!userGamesDictionary.ContainsKey(steamGame.appid))
                {
                    var usergame = new UserGame() { GameId = steamGame.appid, Done = false, PlayedTime = steamGame.playtime_forever };
                    user.UserGames.Add(usergame);
                }
                else
                {
                    user.UserGames.FirstOrDefault(ug => ug.GameId == steamGame.appid).PlayedTime = steamGame.playtime_forever;
                }
            }

            logger.LogInformation("Saving game info for user {userId}", user.Id);
            redis.SetUser(user.Email, user);
            redis.SetGames(games);
            
            var result = games
                .Where(g => userGamesDictionary.ContainsKey(g.GameId) && !userGamesDictionary[g.GameId].Done)
                .Where(g => g.Categories == null || g.Categories.Contains("Single-player"))
                .ToList();

            return result;
        }

        private async Task<GamesResponse> GetGamesForSteamId(string steamId)
        {
            string url = string.Format($"http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={ApiKey}&steamid={steamId}&format=json");
            logger.LogInformation($"Getting games from endpoint {url}");
            var response = await httpClient.GetStringAsync(url);
            var result = JsonConvert.DeserializeObject<GamesResponse>(response);
            return result;
        }
    }
}