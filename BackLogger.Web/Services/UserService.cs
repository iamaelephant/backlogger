using System.Collections.Generic;
using System.Threading.Tasks;
using BackLogger.Web.Data;
using BackLogger.Web.Model;

namespace BackLogger.Web.Services
{
    public class UserService
    {
        private readonly BackloggerRedis redis;

        public UserService(BackloggerRedis redis)
        {
            this.redis = redis;
        } 
        public async Task<User> GetUser(string email)
        {
            var user = await Task.Run(() => redis.GetUser(email));
            if (user != null && user.UserGames == null)
            {
                user.UserGames = new List<UserGame>();
            }

            return user;
        }

        public async Task CreateUser(User user)
        {
            await Task.Run(() => redis.SetUser(user.Email, user));
        }

        public async Task UpdateUser(User user) 
        {
            await Task.Run(() => redis.SetUser(user.Email, user));
        }

        public async Task<User> SetTrelloLists(string userEmail, IDictionary<string, string> lists)
        {
            var user = await GetUser(userEmail);
            user.ListIdBacklog = lists["backlog"];
            user.ListIdDone    = lists["done"];
            user.ListIdPlaying = lists["playing"];

            await UpdateUser(user);
            return user;
        }

        public async Task<User> SetTrelloBoardId(string userEmail, string trelloBoardId)
        {
            var user = await GetUser(userEmail);
            user.TrelloBoardId = trelloBoardId;

            await UpdateUser(user);
            return user;
        }

        public async Task<User> SetSteamId(string userEmail, string steamId) 
        {
            var user = await GetUser(userEmail);
            user.Steam64Id = steamId;

            await UpdateUser(user);
            return user;
        }
    }
}