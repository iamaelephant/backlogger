
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BackLogger.Web.Data;
using BackLogger.Web.Model;
using BackLogger.Web.Responses;
using BackLogger.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BackLogger.Web.Controllers
{
    [Route("api/[controller]")]
    public class GameController : Controller
    {
        private readonly GamesService gamesService;
        private readonly UserService userService;
        private readonly HttpClient httpClient;
        private readonly ILogger<GameController> log;

        // http://store.steampowered.com/api/appdetails?appids=730

        public GameController(GamesService gamesService, UserService userService, HttpClient httpClient, ILogger<GameController> log)
        {
            this.gamesService = gamesService;
            this.userService = userService;
            this.httpClient = httpClient;
            this.log = log;
        }

        [HttpGet("{gameId}")]
        public async Task<IActionResult> Get(int gameId, [FromQuery] string userEmail)
        {
            Game result = null;

            var storedGame = await gamesService.GetGame(gameId);
            log.LogInformation("Fetched stored game: {storedGame != null} for user email {userEmail}", storedGame?.GameId, userEmail);
            if (storedGame == null || string.IsNullOrEmpty(storedGame.Name)) 
            {
                // fetch and store the info
                string url = string.Format($"http://store.steampowered.com/api/appdetails?appids={gameId}");
                log.LogInformation($"Fetching game from url {url}");
                var response = await httpClient.GetStringAsync(url);
                var steamGame = JsonConvert.DeserializeObject<Dictionary<int, GameResponse>>(response);
                var gameResponse = steamGame[gameId];

                if (!gameResponse.Success) 
                {
                    log.LogInformation("No game found with id {gameId}");
                }
                else
                {
                    Game fetchedGame = new Game()
                    {
                        GameId = gameId,
                        Name = gameResponse.Data.Name,
                        HeaderImage = gameResponse.Data.Header_Image,
                        BackgroundImage = gameResponse.Data.Background,
                        Categories = string.Join(";", gameResponse.Data.Categories.Select(cat => cat.Description))
                    };

                    if (storedGame != null)
                    {
                        log.LogInformation($"Game Id {gameId} was stored, hydrating stored game");
                        storedGame.HeaderImage = fetchedGame.HeaderImage;
                        storedGame.Name = fetchedGame.Name;
                        storedGame.Categories = fetchedGame.Categories;
                        storedGame.BackgroundImage = fetchedGame.BackgroundImage;
                        await gamesService.UpdateGame(storedGame);
                    }
                    else
                    {
                        log.LogInformation($"Adding game {fetchedGame.GameId}");
                        await gamesService.AddGame(fetchedGame);
                    }

                    result = fetchedGame;
                }
            }
            else
            {
                result = storedGame;
            }

            if (userEmail != null)
            {
                log.LogInformation("Fetching playtime and saving game {gameId} to user {userEmail}", gameId, userEmail);
                var user = await userService.GetUser(userEmail);
                if (user != null && user.UserGames != null)
                {
                    var userGame = user.UserGames.FirstOrDefault(ug => ug.GameId == result.GameId);
                    result.PlayedTime = userGame.PlayedTime;
                }
                else
                {
                    log.LogWarning("User email {userEmail} was passed in but no user was found", userEmail);
                }
            }
            else
            {
                log.LogInformation("No user email passed in");
            }

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost("{gameId}/{userEmail}/done")]
        public async Task<IActionResult> Done(int gameId, string userEmail)
        {
            log.LogInformation("Setting game {gameId} to done for user {userEmail}", gameId, userEmail);
            var user = await userService.GetUser(userEmail);
            var userGame = user.UserGames.FirstOrDefault(ug => ug.GameId == gameId);
            if (userGame != null)
            {
                userGame.Done = true;
                await userService.UpdateUser(user);
                log.LogInformation("Updated user {userEmail}", userEmail);
                return Ok();
            }
            else
            {
                log.LogInformation("Could not find user {userEmail}", userEmail);
                return NotFound();
            }
        }
    }
}