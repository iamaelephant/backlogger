using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackLogger.Web.Data;
using Newtonsoft.Json.Linq;
using BackLogger.Web.Model;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using BackLogger.Web.Requests;
using BackLogger.Web.Responses;
using BackLogger.Web.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BackLogger.Web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {

        public UserController(UserService userService, GamesService gamesService, HttpClient httpClient, ILogger<UserController> logger) 
        {
            this.userService = userService;
            this.gamesService = gamesService;
            this.httpClient = httpClient;
            this.logger = logger;
        }

        private static string ApiKey = Environment.GetEnvironmentVariable("STEAM_API_KEY");
        private readonly UserService userService;
        private readonly GamesService gamesService;
        private readonly HttpClient httpClient;
        private readonly ILogger<UserController> logger;

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]User user)
        {
            var existing = await userService.GetUser(user.Email);
            // if user exists get user, 
            if (existing != null) 
            {
                // get the games that belong to the user
                if (existing.Steam64Id != null) {
                    var games = await gamesService.GetGamesForUser(existing);
                    existing.Games = games;
                }
                else {
                    existing.Games = new Game[0];
                }
                return Ok(existing);
            }
            else 
            {
                if (user.Email == null) 
                {
                    return BadRequest("Cannot create user without email address");
                }
                // otherwise add user 
                await userService.CreateUser(user);
                user.Games = new Game[0];
                // and return
                System.Console.WriteLine($"Returning {user.Games.Count()} games");
                return Ok(user);
            }
        }

        [HttpPost("{userEmail}/trelloLists")]
        public async Task<IActionResult> Post([FromRoute]string userEmail, [FromBody]IDictionary<string,IDictionary<string,string>> trelloLists)
        {
            var lists = trelloLists["trelloLists"];
            return Ok(await userService.SetTrelloLists(userEmail, lists));
        }

        [HttpPost("{userEmail}/trelloBoard")]
        public async Task<IActionResult> Post([FromRoute]string userEmail, [FromBody]TrelloBoardRequest request)
        {
           var boardId = request.trelloBoardId;
            return Ok(await userService.SetTrelloBoardId(userEmail, boardId));
        }

        [HttpPost("{userEmail}/steamId")]
        public async Task<IActionResult> Post([FromRoute]string userEmail, [FromBody]SteamIdRequest request)
        {
            var user = await userService.SetSteamId(userEmail, request.Steam64Id);
            var games = await gamesService.GetGamesForUser(user);
            user.Games = games;

            return Ok(user);
        }

        [HttpGet("{steamIdInput}/steamProfile")]
        public async Task<IActionResult> GetSteamProfile([FromRoute]string steamIdInput)
        {
            // try getting a steamId from the username
            var steamId = await GetSteamIdFromVanityUrl(steamIdInput);

            // if this fails, maybe the input is a steam ID already, try getting the profile from that
            if (steamId == null)
            {
                steamId = steamIdInput;
            }

            var profile = await GetSteamProfileResponse(steamId);

            if (profile != null)
            {
                return Ok(profile);
            }
            else
            {
                return NotFound();
            }
        }

        private async Task<string> GetSteamIdFromVanityUrl(string vanityUrl)
        {
            var url = $"http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key={ApiKey}&vanityurl={vanityUrl}";
            logger.LogInformation($"Searching for user by vanityurl {vanityUrl}");
            var response = await httpClient.GetStringAsync(url);
            var result = JsonConvert.DeserializeObject<VanityUrlResponse>(response);

            if (result.Response.Success == 1 && !string.IsNullOrEmpty(result.Response.Steamid))
            {
                logger.LogInformation($"Found user {result.Response.Steamid}");
                return result.Response.Steamid;
            }
            else
            {
                logger.LogInformation("Failed to find user");
                return null;
            }
        }

        private async Task<SteamPlayer> GetSteamProfileResponse(string steamId)
        {
            var url = $"http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={ApiKey}&steamids={steamId}";
            logger.LogInformation($"Searching for user {steamId} from {url}");
            var response = await httpClient.GetStringAsync(url);
            var result = JsonConvert.DeserializeObject<SteamProfileResponse>(response);

            if (result.Response.Players.Count > 0)
            {
                var player = result.Response.Players[0];
                logger.LogInformation($"User found with Steam64Id {player.SteamId} and display name ${player.PersonaName}");
                return player;
            }
            else
            {
                return null;
            }
        }
        private class VanityUrlResponse
        {
            public VanityUrlResponseResponse Response { get; set; }
        }

        private class VanityUrlResponseResponse
        {
            public int Success { get; set; }
            public string Steamid { get; set; }
        }

        private class SteamProfileResponse
        {
            public SteamProfileResponseResponse Response { get; set; }
        }

        private class SteamProfileResponseResponse
        {
            public List<SteamPlayer> Players { get; set; }
        }

        private class SteamPlayer
        {
            public string SteamId { get; set; }
            public string PersonaName { get; set; }
            public Uri ProfileUrl { get; set; }
            public Uri AvatarFull { get; set; }
            public Uri Avatar { get; set; }
            public string RealName { get; set; }
            public string LocCountryCode { get; set; }
        }
    }
}