using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BackLogger.Web.Model {
    public class User
    {
        [Key]
        public int Id { get; set;}
        public string Email { get; set; }

        public string Steam64Id { get; set;}

        public string TrelloBoardId { get; set; }

        public string ListIdDone { get; set; }
        public string ListIdPlaying { get; set; }
        public string ListIdBacklog { get; set; }

        public List<UserGame> UserGames { get; set; }
        [NotMapped] public IEnumerable<Game> Games { get; set; }
    }
}
