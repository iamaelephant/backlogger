using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BackLogger.Web.Model
{
    public class Game 
    {
        [Key]
        public int GameId { get; set; }
        public string Name { get; set; }
        public string HeaderImage { get; set; }
        public string BackgroundImage { get; set; }
        public string Categories { get; set; }
        public int PlayedTime { get; set; }
    }
}
