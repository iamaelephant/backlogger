namespace BackLogger.Web.Model
{
    public class UserGame
    {
        public int GameId { get; set; }
        public bool Done { get; set; }
        public int PlayedTime { get; set; }
    }
}
